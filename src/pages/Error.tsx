import { ErrorTemplate } from "components/templates"

export const Error = () => {
  return (
    <ErrorTemplate></ErrorTemplate>
  )
}

export default Error