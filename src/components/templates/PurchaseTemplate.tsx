import { useEffect } from "react";
import { styled } from "styled-components";
import { BsFillCartCheckFill } from "react-icons/bs";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getDanhSachPhongVeThunk } from "store/quanLyDatVe/thunk";
import { quanLyDatVeActions } from "store/quanLyDatVe/slice";
import { Chair } from "types";

export const PurchaseTemplate = () => {
  const params = useParams();
  const { maLichChieu } = params;
  const dispatch = useAppDispatch();
  const { chairList, movieInfo, chooseSeated, chooseSeats } = useSelector(
    (state: RootState) => state.quanLyDatVe
  );

  const renderChairList = () => {
    return chairList.map((seat) => {
      return (
        <ButtonS
          key={seat.stt}
          className={`hover:bg-orange-400 ${
            chooseSeats.find((e: Chair) => e.stt === seat.stt)
              ? "bg-rose-400 text-white"
              : ""
          } ${
            chooseSeated.find((e) => e.stt === seat.stt) || seat.daDat
              ? "bg-blue-400 pointer-events-none"
              : ""
          }`}
          onClick={() => dispatch(quanLyDatVeActions.setSeat(seat))}
        >
          {seat.stt}
        </ButtonS>
      );
    });
  };
  useEffect(() => {
    dispatch(getDanhSachPhongVeThunk(maLichChieu));
  }, []);

  return (
    <div className="pt-[81px]">
      <div className="grid grid-cols-5 gap-4 py-16">
        <div className="col-span-3 border-r-2">
          <h1 className=" text-blue-500 text-center font-bold text-4xl ">Vị Trí Màn hình</h1>
          <div className="grid justify-items-center mt-5">
            <Container />
          </div>
          <div className="w-4/5 mt-5 m-auto grid grid-cols-10 gap-2">
            {renderChairList()}
          </div>
        </div>
        <div className="col-span-2 px-10">
          <div className="text-black mt-14  text-start ">
            <div className="border-b-2 pb-4">
              <h1 className="text-2xl font-bold mb-5 text-zinc-500">
                Thông tin phim bạn đã chọn
              </h1>
              <div className="flex flex-col gap-2 font-600">
                <p>
                  Cụm rạp: <span>{movieInfo?.tenCumRap}</span>
                </p>
                <p>
                  Địa chỉ:<span>{movieInfo?.diaChi}</span>
                </p>
                <p>
                  Rạp: <span>{movieInfo?.tenRap}</span>
                </p>
                <p>
                  Ngày giờ chiếu:<span>{movieInfo?.ngayChieu}~</span>
                  {movieInfo?.gioChieu}
                </p>
                <p>
                  Tên phim: <span>{movieInfo?.tenPhim}</span>
                </p>
              </div>
            </div>
            <div>
              <h1 className="text-2xl font-bold my-5 text-zinc-500">
                Danh sách ghế bạn đã chọn
              </h1>
              <div className="flex items-center ">
                <div className=" h-4 w-4 bg-blue-400"></div>
                <p className="font-bold ml-5">Ghế đã đặt</p>
              </div>
              <div className="flex items-center">
                <div className=" h-4 w-4 bg-rose-400"></div>
                <p className=" font-bold ml-5">Ghế đang chọn</p>
              </div>
              <div className="flex items-center">
                <div className="h-4 w-4 bg-gray-200"></div>
                <p className="font-bold ml-5">Ghế chưa đặt</p>
              </div>
            </div>

            <table className="table-fixed  w-4/5 mt-5 text-center">
              <thead className="border border-slate-400 ">
                <tr>
                  <th className="border border-slate-400 ">
                    Các Ghế bạn đã chọn
                  </th>
                  <th className="border border-slate-400 ">Giá</th>
                  <th>Huỷ</th>
                </tr>
              </thead>
              <tbody className="border border-slate-400 ">
                {chooseSeats.map((seat) => (
                  <tr key={seat.maGhe} className="border border-slate-400 ">
                    <td className="border border-slate-400 ">{seat.tenGhe}</td>
                    <td className="border border-slate-400 ">
                      {seat.giaVe} vnđ
                    </td>
                    <td className="flex items-center justify-center">
                      <button
                        type="button"
                        className=" bg-red-500 hover:bg-red-700 text-white  py-1 px-3 rounded"
                        onClick={() =>
                          dispatch(quanLyDatVeActions.setSeat(seat))
                        }
                      >
                        {" "}
                        Huỷ
                      </button>
                    </td>
                  </tr>
                ))}
                <tr className="border border-slate-600 ">
                  <td className="border border-slate-600 ">Tổng tiền</td>
                  <td className="border border-slate-600 ">
                    {chooseSeats.reduce(
                      (total, seat) => (total += seat.giaVe),
                      0
                    )}
                    vnđ
                  </td>
                </tr>
              </tbody>
            </table>
            <div className=" mt-5">
              <button
                className="text-white bg-gradient-to-r from-sky-400 to-indigo-500 hover:bg-gradient-to-tl  transition duration-500 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 flex gap-2 items-center"
                onClick={() => dispatch(quanLyDatVeActions.setSeatBooked())}
              >
                <BsFillCartCheckFill />
                <span>Thanh toán</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PurchaseTemplate;

const Container = styled.div`
  border-bottom: 50px solid #3b82f6;
  border-left: 50px solid transparent;
  border-right: 50px solid transparent;
  height: 100;
  width: 80%;
  filter: drop-shadow(4px 30px 20px rgba(255, 255, 255, 0.5));
  font-size: 25px;
  color: #fff;
`;
const ButtonS = styled.button`
  border: 3px solid #3b82f6;
  width: 60px;
  height: 40px;
  font-size: 16px;
  border-radius: 5px;
`;
