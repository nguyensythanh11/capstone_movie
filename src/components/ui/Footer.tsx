import { styled } from "styled-components";

export const Footer = () => {
  return (
    <FooterS>
      <div className="Footer-top">
        <div className="flex justify-between">
          <div className="title-left w-[50%]">
            <div className="items-center text-white">
              <h2 className="font-medium text-[35px] ">Cinephile's Paradise</h2>
              <img className="w-[200px] h-[200] ml-[40px] text-white" src="/images/logo-movie1.png" alt="logo" />
            </div>
          </div>
          <div className="title-right w-[50%] pl-[50px]">
            <h1 className="text-[30px] font-medium ">Giới Thiệu </h1>
            <hr />
            <ul className="!mt-[20px]">
              <li>
                <a href="#">Về Chúng Tôi</a>
              </li>
              <li>
                {" "}
                <a href="#">Thỏa Thuận Khoản Sử Dụng</a>
              </li>
              <li>
                <a href="#">Quy Chế Hoạt Động</a>
              </li>
              <li>
                <a href="#">Chính Sách Bảo Mật</a>
              </li>
            </ul>
          </div>
          <div className="title-right w-[50%] pl-[100px] ">
            <h1 className="text-[30px] font-medium ">Hỗ Trợ </h1>
            <hr />
            <ul className="!mt-[20px]">
              <li>
                <a href="#">Góp Ý</a>
              </li>
              <li>
                <a href="#">Sale And Sevices</a>
              </li>
              <li>
                <a href="#">Rạp / Giá Vé</a>
              </li>
              <li>
                <a href="#">Tuyển Dụng</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="Footer-bottom">
        <p className="text-center"  >
          <a href="#">
          Copyright © 2023 Chuc Nguyen and Thanh Nguyen cyberSoft
          </a>
        </p>
      </div>
    </FooterS>
  );
};

export default Footer;
const FooterS = styled.footer`
  background-color: #71717a;
  height: 250px;
  
  .Footer-top {
    max-width: var(--max-width);
    margin: auto;
    padding-top: 10px;
    /* padding-bottom: 50px; */
    .title-right {
      h1 {
        color: rgb(255, 255, 255);
      }
      li {
        padding-bottom: 15px;
        a {
          color: #eff6ff;
          font-size: 18px;
          font-weight: 400;
        }
        a:hover {
          color: #ec4899;
        }
      }
      hr {
        max-width: 200px;
        border: 1px solid #eff6ff;
      }
    }
  }
  .Footer-bottom {
    background-color: #2d2a2a;
    color: rgba(255, 255, 255, 0.7);
    p {
      max-width: var(--max-width);
      margin: auto;
      padding-top: 10px;
    }
  }
`;
