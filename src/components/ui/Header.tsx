import { NavLink, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Avatar, Popover } from ".";
import { UserOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { useDispatch } from "react-redux";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
import { useAuth } from "hooks";

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { user } = useAuth();
  const accessTokenJson = localStorage.getItem('accessToken');
  if(accessTokenJson){
    var accessToken = JSON.parse(accessTokenJson)
  }
  return (
    <HeaderS>
      <div className="header-content">
        <h2
          className="font-600 text-[30px] cursor-pointer uppercase  hover:text-pink-500 rounded-lg transition-all duration-300 "
          onClick={() => {
            navigate("/");
          }}
        >
          Cinephiles Paradise
        </h2>
        <div className="flex items-center justify-around">
          <NavLink className="mr-40 font-600  hover:text-pink-500 rounded-lg transition-all duration-300" to={PATH.home}>
            Home
          </NavLink>
          <NavLink className="mr-40 font-600  hover:text-pink-500 rounded-lg transition-all duration-300" to={PATH.purchase}>
            Purchase
          </NavLink>
          <NavLink className="font-600  hover:text-pink-500 rounded-lg transition-all duration-300" to={PATH.contact}>Contact</NavLink>
          {accessToken && (
            <Popover
              content={
                <div className="p-2">
                  <h2 className="font-600 mb-4 p-2">{user?.hoTen}</h2>
                  <hr />
                  <div
                    onClick={() => {
                      navigate(PATH.account);
                    }}
                    className="p-2 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                  >
                    Thông tin tài khoản
                  </div>
                  <div
                    className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                    onClick={() => {              
                      dispatch(quanLyNguoiDungActions.logOut());
                      navigate(PATH.login)
                    }}
                  >
                    Đăng xuất
                  </div>
                </div>
              }
              trigger={"click"}
            >
              <Avatar
                className="!ml-40 hover:cursor-pointer !flex !items-center !justify-center"
                size={30}
                icon={<UserOutlined></UserOutlined>}
              ></Avatar>
            </Popover>
          )}
          {!accessToken && (
            <NavLink className="ml-40 font-600" to={PATH.login}>
              Login
            </NavLink>
          )}
          {!accessToken && (
            <NavLink className="ml-40 font-600" to={PATH.register}>
              Register
            </NavLink>
          )}
        </div>
      </div>
    </HeaderS>
  );
};

export default Header;

const HeaderS = styled.header`
  height: var(--header-height);

  background: #71717a;
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1;
  .header-content {
    display: fixed;
    color: white;
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
  }
`;
