import { Carousel as CarouselA, CarouselProps as CarouselPropsA } from 'antd'

export const Carousel = (props: CarouselPropsA) => {
  return (
    <CarouselA {... props}></CarouselA>
  )
}

export default Carousel