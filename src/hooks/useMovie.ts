import { useSelector } from "react-redux"
import { RootState } from "store"

export const useMovie = () => { 
    const { movieList, bannerList, isFetchingMovieList, currentPage } = useSelector((state: RootState) => state.quanLyPhim);
    return { movieList, bannerList, isFetchingMovieList, currentPage };
}