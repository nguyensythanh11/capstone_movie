import { apiInstance } from "constant";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_DAT_VE_API,
});
export const quanLyPhongVeService = {
  getDanhSachPhongVe: (query: string) =>
    api.get<ApiResponses<any>>(`/LayDanhSachPhongVe${query}`),
};
