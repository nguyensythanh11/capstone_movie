export const PATH = {
    home: '/',
    login: '/login',
    register: '/register',
    about: '/about',
    contact: '/conatct',
    account: '/account',
    detailmovie: '/detailmovie/:movieId',
    purchase: '/purchase/:maLichChieu',
}