import { createSlice } from "@reduxjs/toolkit";
import { Banner, MoviePage } from "types";
import { getBannerListThunk, getMovieListThunk } from "./thunk";

type QuanLyPhimState = {
    movieList?: MoviePage,
    isFetchingMovieList?: boolean,
    bannerList?: Banner[],
    currentPage: number;
}

const initialState: QuanLyPhimState = {
    movieList: undefined,
    isFetchingMovieList: false,
    bannerList: [],
    currentPage: 1,
}

const quanLyPhimSlice = createSlice({
    name: 'QuanLyPhim',
    initialState,
    reducers: {setCurrentPage: (state, action) => {
        state.currentPage = action.payload;
      },},
    extraReducers: (builder) => { 
        builder
            .addCase(getMovieListThunk.pending, (state) => {
                state.isFetchingMovieList = true;
            })
            .addCase(getMovieListThunk.fulfilled, (state, {payload}) => { 
                state.movieList = payload;
                state.isFetchingMovieList = false;
            })
            .addCase(getMovieListThunk.rejected, (state) => {
                state.isFetchingMovieList = false;
            })
            .addCase(getBannerListThunk.fulfilled, (state, {payload}) => {
                state.bannerList = payload;
            })
    }
})

export const { reducer: quanLyPhimReducer, actions: quanLyPhimActions } = quanLyPhimSlice;